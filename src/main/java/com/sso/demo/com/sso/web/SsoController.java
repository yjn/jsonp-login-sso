package com.sso.demo.com.sso.web;

import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wxm
 * @description
 * @date 2020/8/20
 */
@RestController
@Scope(value = "request")
@RequestMapping(value = "/")
public class SsoController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index() {
        ModelAndView mv = new ModelAndView("index");
        return mv;
    }

    @RequestMapping(value = "/set_cookie", method = RequestMethod.GET)
    @ResponseBody
    public void set_cookie(@RequestParam(defaultValue = "") String domain,@RequestParam(defaultValue = "") String token,  HttpServletRequest request, HttpServletResponse response) {
            System.out.println(domain);

            Cookie cookie = new Cookie("ssoCookie", token);
            cookie.setPath("/");
            cookie.setDomain(domain);
            response.addCookie(cookie);



/*        String callback = request.getParameter("callback");
        System.out.println(callback);
        StringBuffer aa = new StringBuffer("");
        aa.append("var oSc = document.createElement('script');");
        aa.append("oSc.src = 'http://\"+domain+\"/set_cookie?domain=\"+domain+\"&token=\"+token+\"';");
        aa.append("document.getElementsByTagName('head')[0].appendChild(oSc);");*/
        try {
            response.getWriter().write("test");
        } catch (Exception e) {

        }

    }
}
